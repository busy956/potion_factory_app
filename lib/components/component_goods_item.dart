import 'package:flutter/material.dart';
import 'package:potion_factory_app/model/goods_item.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem({
    super.key,
    required this.goodsItem,
    required this.callback
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Row(
          children: [
            SizedBox(
              child: Image.asset(goodsItem.imgUrl),
            ),
            Column(
              children: [
                Text(goodsItem.goodsTitle),
                Text('${goodsItem.goodsPrice}')
              ],
            ),
          ],
        ),
      ),
    );
  }
}
