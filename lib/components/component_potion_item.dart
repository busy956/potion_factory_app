import 'package:flutter/material.dart';

class ComponentPotionItem extends StatelessWidget {
  const ComponentPotionItem({
    super.key,
    required this.imgSrc,
    required this.goodsTitle,
    required this.goodsPrice,
    required this.callback,
  });

  final String imgSrc;
  final String goodsTitle;
  final String goodsPrice;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        margin: const EdgeInsets.fromLTRB(17, 10, 17, 0),
        child: Column(
          children: [
            Image.asset(imgSrc,
              height: 100,
              width: 100,
            ),
            Text(goodsTitle,
            style: const TextStyle(
              fontSize: 15,
              color: Colors.white,
            ),
            ),
            Text('$goodsPrice G',
              style: const TextStyle(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
