import 'package:flutter/material.dart';
import 'package:potion_factory_app/model/potion_item.dart';

class ComponentPotionsItem extends StatelessWidget {
  const ComponentPotionsItem({
    super.key,
    required this.potionItem,
    required this.callback
  });

  final PotionItem potionItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Row(
          children: [
            SizedBox(
              child: Image.asset(potionItem.imgSrc),
            ),
            Column(
              children: [
                Text(potionItem.potionTitle),
                Text('${potionItem.potionPrice} G'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
