class PotionItem {
  num id;
  String imgSrc;
  String potionTitle;
  num potionPrice;

  PotionItem(this.id, this.imgSrc, this.potionTitle, this.potionPrice);
}
