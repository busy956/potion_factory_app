import 'package:flutter/material.dart';

class PagePotionDetail extends StatelessWidget {
  const PagePotionDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: const Color.fromRGBO(40, 19, 69, 1),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            color: const Color.fromRGBO(40, 19, 69, 1),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Container(
                  child: Image.asset('assets/logo.png',
                    width: 200,
                    height: 150,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Image.asset('assets/red_1.png',
                              width: 200,
                              height: 200,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Column(
                            children: [
                              Container(
                                child: const Text('차갑다,',
                                  style: TextStyle(
                                    fontFamily: 'BlackHanSans',
                                    fontSize: 40,
                                    color: Colors.blue,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text('부드럽다',
                                  style: TextStyle(
                                    fontFamily: 'BlackHanSans',
                                    fontSize: 40,
                                    color:Colors.cyan,
                                  ),
                                ),
                              ),
                              Container(
                                child: const Text('& 맛있다',
                                  style: TextStyle(
                                    fontFamily: 'BlackHanSans',
                                    fontSize: 40,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  child: Image.asset('assets/detail.png',
                    width: 380,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(60, 0, 20, 10),
                        child: OutlinedButton(
                          onPressed: () {},
                          child: const Text('장바구니',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(50, 0, 20, 10),
                          child: OutlinedButton(
                            onPressed: () {},
                            child: const Text('구매하기',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
