import 'package:flutter/material.dart';
import 'package:potion_factory_app/model/potion_item.dart';

class PagePotionsDetail extends StatefulWidget {
  const PagePotionsDetail({
    super.key,
    required this.potionItem
  });

  final PotionItem potionItem;

  @override
  State<PagePotionsDetail> createState() => _PagePotionsDetailState();
}

class _PagePotionsDetailState extends State<PagePotionsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('포션 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Text('${widget.potionItem.id}'),
              Image.asset(widget.potionItem.imgSrc),
              Text(widget.potionItem.potionTitle),
              Text('${widget.potionItem.potionPrice}')
            ],
          ),
        ),
      ),
    );
  }
}
