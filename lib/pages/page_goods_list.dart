import 'package:flutter/material.dart';
import 'package:potion_factory_app/components/component_goods_item.dart';
import 'package:potion_factory_app/model/goods_item.dart';
import 'package:potion_factory_app/pages/page_goods_detail.dart';

class Page extends StatefulWidget {
  const Page({super.key});

  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  List<GoodsItem> _list = [
    GoodsItem(1,'assets/1.jpg', '맛있는 메추리알 30개입', 1000),
    GoodsItem(2,'assets/2.jpg', '물티슈', 500),
    GoodsItem(3,'assets/3.jpg', '생수 2L', 300),
    GoodsItem(4,'assets/4.jpg', '맛있는 메추리알 30개입', 1000),
    GoodsItem(5,'assets/5.jpg', '물티슈', 500),
    GoodsItem(6,'assets/6.jpg', '생수 2L', 300),
    GoodsItem(7,'assets/7.jpg', '맛있는 메추리알 30개입', 1000),
    GoodsItem(8,'assets/8.jpg', '물티슈', 500),
    GoodsItem(9,'assets/9.jpg', '생수 2L', 300),
    GoodsItem(10,'assets/10.jpg', '맛있는 메추리알 30개입', 1000),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      appBar: AppBar(
        title: const Text('상품 리스트'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return ComponentGoodsItem(
              goodsItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(goodsItem: _list[idx])));
              },
            );
          },
        ),
      ),
    );
  }
}
