import 'package:flutter/material.dart';
import 'package:potion_factory_app/components/component_potions_item.dart';
import 'package:potion_factory_app/model/potion_item.dart';
import 'package:potion_factory_app/pages/backup2.dart';
import 'package:potion_factory_app/pages/page_potions_detail.dart';

class PagePotionList extends StatefulWidget {
  const PagePotionList({super.key});

  @override
  State<PagePotionList> createState() => _PagePotionListState();
}

class _PagePotionListState extends State<PagePotionList> {
  List<PotionItem> _list = [
    PotionItem(1, 'assets/red_1.png', '하급 레드포션', 500),
    PotionItem(2, 'assets/red_2.png', '중급 레드포션', 1000),
    PotionItem(3, 'assets/red_3.png', '상급 레드포션', 1500),
    PotionItem(4, 'assets/blue_1.png', '하급 블루포션', 500),
    PotionItem(5, 'assets/blue_2.png', '하급 블루포션', 1000),
    PotionItem(6, 'assets/blue_3.png', '하급 블루포션', 1500),
    PotionItem(7, 'assets/purple.png', '퍼플포션', 500),
    PotionItem(8, 'assets/green.png', '그린포션', 1000),
    PotionItem(9, 'assets/yellow.png', '옐로우포션', 1500),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('물약 리스트'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return Container(
              color: Colors.yellow,
              child: ComponentPotionsItem(
                potionItem: _list[idx],
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePotionsDetail(potionItem: _list[idx])));
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
