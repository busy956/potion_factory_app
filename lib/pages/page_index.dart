import 'package:flutter/material.dart';
import 'package:potion_factory_app/components/component_potion_item.dart';
import 'package:potion_factory_app/pages/page_potion_detail.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  }
  Widget _buildBody(BuildContext context) {
    return Container(
      child: Container(
        color: const Color.fromRGBO(40, 19, 69, 1),
        child: Column(
          children: [
            Container(
              child: Image.asset('assets/logo.png',
                width: 200,
                height: 200,
                fit: BoxFit.fill,
              ),
            ),
            Container(
              child: Image.asset('assets/advertise.png',
                width: 1000,
                fit: BoxFit.contain,
              ),
            ),
            Expanded(
              child: GridView(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, //열 최대 아이템 개수
                  mainAxisSpacing: 10,  //아이템의 세로 간격
                  crossAxisSpacing: 10, //아이템의 가로 간격
                  childAspectRatio: 1/2,
                ),
                children: [
                      ComponentPotionItem(
                        imgSrc: 'assets/red_1.png',
                        goodsTitle: '하급 레드포션',
                        goodsPrice: '500',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/red_2.png',
                        goodsTitle: '중급 레드포션',
                        goodsPrice: '1000',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/red_3.png',
                        goodsTitle: '상급 레드포션',
                        goodsPrice: '1500',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/blue_1.png',
                        goodsTitle: '하급 블루포션',
                        goodsPrice: '500',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/blue_2.png',
                        goodsTitle: '중급 블루포션',
                        goodsPrice: '1000',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/blue_3.png',
                        goodsTitle: '상급 블루포션',
                        goodsPrice: '1500',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/purple.png',
                        goodsTitle: '퍼플포션',
                        goodsPrice: '1000',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/green.png',
                        goodsTitle: '그린포션',
                        goodsPrice: '1000',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                      ComponentPotionItem(
                        imgSrc: 'assets/yellow.png',
                        goodsTitle: '옐로포션',
                        goodsPrice: '1000',
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePotionDetail()));
                        },
                      ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
