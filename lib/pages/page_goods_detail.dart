import 'package:flutter/material.dart';
import 'package:potion_factory_app/model/goods_item.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({
    super.key,
    required this.goodsItem
  });

  final GoodsItem goodsItem;

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Container(
            child: Column(
              children: [
                Text('${widget.goodsItem.id}'),
                Image.asset(widget.goodsItem.imgUrl),
                Text(widget.goodsItem.goodsTitle),
                Text('${widget.goodsItem.goodsPrice}'),
              ],
            ),
        ),
      ),
    );
  }
}
